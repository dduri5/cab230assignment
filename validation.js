function Validate_User(email, password, confirm_email){
	var email_patt = /^[A-z0-9]{1,20}(@)[A-z]+(.)[A-z]{2,3}/;
	var password_patt = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.!@#\$%\^&\*])(?=.{8,})/;
	if (!email_patt.test(email)){
		alert("Invalid Email!");
		return false;
	} else if (email != confirm_email){
		alert("Email does not match");
		return false;
	} else if (!password_patt.test(password)){
		alert("Invalid Password!");
		return false;
	}
	else {
		return true;
	}
} 

//window.onload = function(){
//	form = document.getElementById('register');
//	form.onsubmit = function(){
//		return Validate_User(email.value, password.value, confirm_email.value);
//	};
//};