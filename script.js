
///////// Functions ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function advanced_search_access(){ // Function for expanding/contracting the header section for the advanced search
	
	var search_section = document.getElementById("search_section"); //Retrieve search_section div: the section that contains the search bar and advanced search
	var advanced_search = document.getElementById("advanced_search_link"); //Retrieve the link that is titled advanced_search: when the user presses this link they
																		   //will be presented with the advanced search
	
	advanced_search.onclick = function(){ // When the advanced search button is clicked
		if(search_section.className == "advanced_search_clicked"){ //If the advanced search is open, then close it and change the link text back to advanced search
			//close advanced search
			search_section.className = "";
			advanced_search.innerHTML = "Advanced Search"; 
			
		} else { //Otherwise (if the advanced search is closed), then expand the advanced search and change the link text to indicate the button will also collapse the advanced search section
			// open advanced search
			search_section.className = "advanced_search_clicked";
			advanced_search.innerHTML = "Close";
		}
	};
}


function getLocation() { //Function outputting results of showPosition or show_Error function if geolocation is supported
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, showError);
	} else {
		document.getElementById("status").innerHTML="Geolocation is not supported by this browser.";
	}
}

function showPosition(position) { // retrieves and outputs map data and coordinates
	document.getElementById("status").innerHTML = "Latitude: " + position.coords.latitude + ", Longitude: " + position.coords.longitude; // Text is modified to display coordinates

	// display on a map
	var latlon = -27.37893 + "," + 153.04461; //temporary hard-coded coordinates for display purposes only
	var img_url = "http://maps.googleapis.com/maps/api/staticmap?center="+latlon+"&zoom=14&size=600x800&sensor=false"; //retrieve mapdata from website with given coordinates and hard-coded dimensions
	document.getElementById("mapholder").innerHTML = "<img src='"+img_url+"'>"; //output mapdada retrieved above
	
}
function showError(error) { //error checking
	var msg = "";
	switch(error.code) {
		case error.PERMISSION_DENIED:
			msg = "User denied the request for Geolocation.";
			break;
		case error.POSITION_UNAVAILABLE:
			msg = "Location information is unavailable.";
			break;
		case error.TIMEOUT:
			msg = "The request to get user location timed out.";
			break;
		case error.UNKNOWN_ERROR:
			msg = "An unknown error occurred.";
			break;
	}
	document.getElementById("status").innerHTML = msg; //if there is an error, status text will display appropriate message.
}

///// Events //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

window.onload = function(){
	getLocation(); 
	advanced_search_access();
	};
	
	
	
	
	
	